Dapp System

A Proof of Concept dapp architecture meant as an educational tool to learn about the different components in a dapp including

##Blockchain

A solidity smart contract that allows for creation of and voting on proposals

##Indexer

A service that polls the blockchain for new blocks, and sends updats to the app server

##App Server

Centralized app server for the dapp. Ingests updates from the chain or any other source. Provides an interface for clients to query.

##Client

Web app that supports viewing of dapp data and initiating transactions to be signed by metamask

#How To Run
truffle is required to run this application

all commands should be run from the project root

install the required packages with yarn

    yarn

3 services are required to run the dapp system; blockchain and smart contracts, chain poller, and the app server/client

to run the blockchain and deploy the contracts

    cd contracts

    truffle develop

    compile

    deploy


to run the app server client

    yarn dev


to run the chain poller

    cd chain_poller
    node index.js


in a browser navigate to http://localhost:3000/ to interact with the dapp

the dapp runs a datastore that saves chain updates to a local file, if the chain is restarted the dataStore file must also be deleted
    
    rm store.json


confirmed to work with the

    Truffle v5.1.11 (core: 5.1.11)
    Solidity v0.5.16 (solc-js)
 