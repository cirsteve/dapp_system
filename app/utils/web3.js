import { ethers } from 'ethers'
import Web3 from 'web3'
const ProposalVotingArtifact = require("../../contracts/build/contracts/Proposals.json")

const GANACHE_NETWORK_ID = '5777'


export const getProvider = async () => {
    if (window.ethereum) {
        try {
            // Request account access if needed
            await ethereum.enable();
            //return new ethers.providers.Web3Provider(ethereum)
            return new Web3(ethereum)
        } catch (error) {
            alert('you must grant this site permission to interact')
        }
    }
    // Legacy dapp browsers...
    else if (window.web3) {
        //return  new ethers.providers.Web3Provider(web3.currentProvider);
        return new Web3(web3.currentProvider)
    }
    // Non-dapp browsers...
    else {
        console.log('Non-Ethereum browser detected. You should consider trying MetaMask!');
    }
}

export const getProposalVotingInstance = async () => {
    const provider = await getProvider()
    console.log('prov is ', provider, ProposalVotingArtifact.networks[GANACHE_NETWORK_ID].address)
    if (provider) {
        return new provider.eth.Contract(
            ProposalVotingArtifact.abi,
            ProposalVotingArtifact.networks[GANACHE_NETWORK_ID].address
             )
        /*
        return new ethers.Contract(
            ProposalVotingArtifact.networks[GANACHE_NETWORK_ID].address,
            ProposalVotingArtifact.abi,
            provider.getSigner()
        )
        */
    }
}