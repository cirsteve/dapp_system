const store = require('data-store')({ path: process.cwd() + '/store.json' })
const PROPOSALS = 'proposals'
const VOTING_RECORD = 'votingRecord'
const initStore = () => {
    console.log('initing store ', store.get(PROPOSALS), store.get(VOTING_RECORD))
    if (!store.get(PROPOSALS)) {
        console.log('setting proposals')
        store.set(PROPOSALS, [])
    }
}
initStore()

const getProposals = () => store.get(PROPOSALS)

const getProposal = (id) => getProposals().find(proposal => proposal.id === id)

const addProposal = (proposal) => {
    const proposals = [...store.get(PROPOSALS)]
    proposals.push(proposal)
    store.set(PROPOSALS, proposals)
    store.set(`${VOTING_RECORD}.${proposal.id}`, {})
}

const getVotingRecord = (id, address) => store.get(`${VOTING_RECORD}.${id}.${address}`)

const addVote = (vote) => {
    console.log('adding vote ', vote)
    const proposals = [...store.get(PROPOSALS)]
    const proposalIndex = proposals.findIndex(proposal => proposal.id === vote.proposalId)
    const proposal = {...proposals[proposalIndex]}

    // if vote.vote is true its a vote for the proposal
    if (vote.vote) {
        proposal.forVotes++
    } else {
        proposal.againstVotes++
    }

    proposals.splice(proposalIndex, 1, proposal)

    store.set(PROPOSALS, proposals)
    store.set(`${VOTING_RECORD}.${vote.proposalId}.${vote.voter}`, true)
}

module.exports = {
    getProposals,
    getProposal,
    addProposal,
    getVotingRecord,
    addVote
}