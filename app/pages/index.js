import { useQuery } from 'graphql-hooks'
import Layout from '../components/layout/Layout'
import CreateProposal from '../components/forms/CreateProposal'
import ProposalsList from '../components/proposals/List'

const PROPOSALS_QUERY = `{
  proposals{
    id
    createdBy
    name
    endingTime
    ended
    forVotes
    againstVotes
  }
}`

export default function () {
  const { loading, error, data } = useQuery(PROPOSALS_QUERY)
  console.log('error ', error)
  console.log('isloading ', loading)
  console.log(`got data ${JSON.stringify(data)}`)
  return (
    <Layout>
      <h2>Proposals</h2>
      <CreateProposal />
      {loading? 
      'Loading...' : <ProposalsList proposals={data.proposals} /> }
    </Layout>
  )
}