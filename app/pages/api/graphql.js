import { graphql, buildSchema } from "graphql";
const dataStore = require('../../utils/dataStore')

const schema = buildSchema(`
  type Proposal {
    id: String
    createdBy: String
    name: String
    endingTime: String
    ended: Boolean
    forVotes: Int
    againstVotes: Int
  }
  
  input LogInput {
    blockHeight: Int
    blockTime: Int
    txHash: String!
    name: String!
    args: String
    address: String!

  }

  input BlockUpdateInput {
    blockHash: String!
    blockHeight: Int!
    logs: [LogInput!]!
  }

  type Query {
    proposals: [Proposal!]!
    proposal(id: String): Proposal
    votingRecord(proposalId: String, address: String): Boolean
  }

  type Mutation {
    updateBlock(blockUpdate: BlockUpdateInput): Boolean
  }
`)

const createProposal = (proposal) => {
  const fullProposal = {...proposal, forVotes: 0, againstVotes: 0}
  console.log(`adding proposal ${JSON.stringify(fullProposal)}`)
  dataStore.addProposal(fullProposal)
}

const addVote = (vote) => {
  console.log('adding vote ', vote)
  dataStore.addVote(vote)
}

const processLog = (log) => {
  console.log('processing log ', log)
  const args = JSON.parse(log.args).reduce((acc, arg) => {
    acc[arg.name] = arg.value
    return acc
  }, {})
  switch (log.name) {
    case 'ProposalCreated':
      createProposal(args)
      break
    case 'VoteForProposal':
      addVote(args)
      break
    default:

  }
}

const addProposalDerivedValues = (proposal) => {
  const endingTime = new Date(1*proposal.endingTime).toLocaleDateString() 
  return {
    ...proposal,
    ended: proposal.endingTime < new Date().getTime(),
    endingTime
  }
}

const root = {
  proposals: () => dataStore.getProposals().map(addProposalDerivedValues),
  proposal: ({id}) => addProposalDerivedValues(dataStore.getProposal(id)),
  votingRecord: ({proposalId, address}) => dataStore.getVotingRecord(proposalId, address),
  updateBlock: ({blockUpdate}) => {
    blockUpdate.logs.forEach(processLog)
    return true
  }
};

export default async (req, res) => {
  const { variables, query } = req.body;
  try {
    const response = await graphql(schema, query, root, {}, variables)
    return res.send(JSON.stringify(response));
  } catch(e) {
    console.log('error in gql response ', e.message)
  }
};