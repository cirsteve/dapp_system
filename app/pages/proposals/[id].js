import { useQuery } from 'graphql-hooks'
import { useRouter } from 'next/router'
import Layout from '../../components/layout/Layout'
import Detail from '../../components/proposals/Detail'

const PROPOSAL_QUERY = `
  query getProposal($id: String) {
    proposal(id: $id){
      id
      createdBy
      name
      endingTime
      ended
      forVotes
      againstVotes
    }
  }
`

export default function () {
  const router = useRouter()
  const { id } = router.query
  const { loading, error, data } = useQuery(PROPOSAL_QUERY, {variables:{id}})

  return (
    <Layout>
      {loading? 
      'Loading...' : <Detail {...data.proposal} />}
    </Layout>
  )
}