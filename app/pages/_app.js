import 'typeface-roboto'
import React from 'react'
import { GraphQLClient, ClientContext } from 'graphql-hooks'
import fetch from "cross-fetch"

const client = new GraphQLClient({
  url: '/api/graphql',
  fetch
})

export default function ({ Component, pageProps }) {
  return (
    <ClientContext.Provider value={client}>
      <Component {...pageProps} />
    </ClientContext.Provider>
  )
}