import React from 'react'
import TextField from '@material-ui/core/TextField'

export default props =>
  <TextField
    key={props.key}
    autoFocus={props.autoFocus}
    onChange={props.onChange}
    id={props.name}
    label={props.label}
    type={props.type}
    fullWidth={props.fullWidth}
    value={props.value} />