import React from 'react';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';

const RadioInput = ({value, label}) => 
    <FormControlLabel
        value={value}
        control={<Radio color="primary" />}
        label={label}
        labelPlacement="top"
    />

export default function({value, onChange, options}) {
  console.log('radio in ', value, options)
  return (
    <FormControl component="fieldset">
      <FormLabel component="legend">labelPlacement</FormLabel>
      <RadioGroup aria-label="position" name="position" value={value} onChange={onChange} row>
        {options.map(option => <RadioInput key={option.value} value={option.value} label={option.label} />)}
      </RadioGroup>
    </FormControl>
  );
}
