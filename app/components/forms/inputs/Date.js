import 'date-fns';
import React from 'react';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardTimePicker,
  KeyboardDatePicker,
} from '@material-ui/pickers';

const TimePicker = ({date, onChange}) =>
    <KeyboardTimePicker
        margin="normal"
        id="time-picker"
        label="Time picker"
        value={date}
        onChange={onChange}
        KeyboardButtonProps={{
            'aria-label': 'change time',
        }} 
    />
    
export default function MaterialUIPickers({value, label, onChange}) {
  console.log('got date value ', value)
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <Grid container justify="space-around">
        <KeyboardDatePicker
          margin="normal"
          id="date-picker-dialog"
          label={label}
          format="MM/dd/yyyy"
          value={new Date(value)}
          onChange={onChange}
          KeyboardButtonProps={{
            'aria-label': 'change date',
          }}
        />
      </Grid>
    </MuiPickersUtilsProvider>
  );
}
