import React from 'react'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';

export default class FormDialog extends React.Component {

  render() {
    const {text, content, onSubmit, submitLabel, submitted } = this.props
    return (
      <div>
        <Dialog
          open={this.props.open}
          onClose={this.props.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogContent>
            { submitted ?
                <DialogContentText>
                    Complete your submission with metamask
                </DialogContentText>
                :
                <React.Fragment>
                <DialogContentText>
                {text}
                </DialogContentText>
                {content}
                </React.Fragment>
            }
          </DialogContent>
          <DialogActions>
            { submitted ?
                <Button onClick={this.props.handleClose} color="primary">
                    Close
                </Button>
                :
                <React.Fragment>
                    <Button onClick={this.props.handleClose} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={onSubmit} color="primary">
                        {submitLabel}
                    </Button>
                </React.Fragment>
                

            }
          </DialogActions>
        </Dialog>
      </div>
    );
  }
}