import React, { Component, Fragment } from 'react'
import Dialog from './Dialog'
import Button from './inputs/Button'
import Text from './inputs/Text'
import Radio from './inputs/Radio'
import Date from './inputs/Date'


export const INPUT_MAP = {
    text: props => <Text {...props} />,
    radio: props => <Radio {...props} />,
    date: props => <Date {...props} />,
  }

class ModalForm extends Component {
  constructor (props) {
    super(props)

    this.state = {
      open: false,
      submitted: false,
      fields: this.props.schema.reduce((acc, f) => {
        acc[f.name] = f.default;
        return acc;
      }, {})
    }
  }

  updateShowingForm = open => this.setState({...this.state, submitted: false, open})

  updateField = (field, e, val) => {
    this.setState({
      ...this.state,
      fields: {
        ...this.state.fields,
        [field]: val || e.target.value
      }
    })
  }

  submit = async () => {
    this.formSubmitted()
    await this.props.submit(this.state.fields)
    this.setState({...this.state, open:false})
  }

  formSubmitted () {
    const fields = this.props.schema.reduce((acc, f) => {
      acc[f.name] = f.default;
      return acc;
    }, {})
    this.setState({...this.state, submitted: true,  fields})
  } 

  render () {
    const { schema, options, closed } = this.props

    const fields = [...schema].map(f => {
      f.value = this.state.fields[f.name]
      f.onChange = this.updateField.bind(this, f.name)
      f.key = f.name
      f.options = f.options
      return f;
    })

    const fieldInputs = fields.map(f => INPUT_MAP[f.type](f) )

    const formFields = 
        <div>
            {fieldInputs}
        </div>

    return (
      <Fragment>

        <Button
            variant="contained"
            onClick={this.updateShowingForm.bind(this, true)}
            color="primary"
            text={this.props.buttonText} />

        <Dialog
            title={this.props.dialogFormTitle}
            open={this.state.open}
            text={this.props.dialogText}
            handleClose={this.updateShowingForm.bind(this, false)}
            submitLabel={this.props.submitLabel}
            onSubmit={this.submit}
            content={formFields}
            submitted={this.state.submitted} />

      </Fragment>
    )
  }
}

export default ModalForm