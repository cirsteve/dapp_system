  import React from 'react'
  import Form from './BaseForm'
  import { getProposalVotingInstance } from '../../utils/web3'
  
  const schema = [
    {
        name: 'vote',
        label: 'Vote',
        type: 'radio',
        options: [
            {
                label: 'Yes',
                value: 'yes'
            },
            {
                label: 'No',
                value: 'no'
            }
        ],
        default: ''
    }
  ]
  
const vote = (proposalId) => async (value) => {
    const instance = await getProposalVotingInstance()

    const vote = value.vote === 'yes' ? true :  false
    console.log('voting' , proposalId, vote)
    try {
        const tx = await instance.methods.voteOnProposal(proposalId, vote).send({from:instance.currentProvider.selectedAddress })
        console.log('creat tx ', tx)
    } catch (e) {
        console.log('error submiting create proposal ', JSON.stringify(e.message))
    }
}


export default ({proposalId}) => 
    <Form
        schema={schema}
        title='Vote on Proposal'
        text=''
        buttonText='Vote'
        submitLabel='Submit Vote'
        submit={vote(proposalId)} />