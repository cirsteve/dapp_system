import React from 'react'
import Form from './BaseForm'
import { getProposalVotingInstance } from '../../utils/web3'

const schema = [
    {
      name: 'name',
      label: 'Title',
      type: 'text',
      default: ''
    },
    {
        name: 'endingTime',
        label: 'Ending Time',
        type: 'date',
        default: new Date().toString()
      }
  ]

  const createProposal = async (values) => {
      console.log('creating proposal with values' , JSON.stringify(values))
      const instance = await getProposalVotingInstance()
      const endingTime = new Date(values.endingTime).getTime()
      const name = values.name
      console.log(instance.currentProvider, name, endingTime)

      try {
        const tx = await instance.methods.createProposal(endingTime, name).send({from:instance.currentProvider.selectedAddress })
        console.log('creat tx ', tx)
      } catch (e) {
          console.log('error submiting create proposal ', JSON.stringify(e.message))
      }
  }

  
  export default () => 
    <Form
        schema={schema}
        title='Create a Proposal for Voting'
        text='Enter the title of the proposal and the ending time for voting. As soon as you create the proposal it will be live.'
        buttonText='Create Proposal'
        submitLabel='Submit Proposal'
        submit={createProposal} />