import React, {useState, useEffect} from 'react'
import { getProvider as _getProvider } from '../../utils/web3'
import Vote from './Vote'


const VoteCount = ({label, count}) => 
    <div>
        <div>
            {label} - {count}
        </div>
    </div>

export default ({id, name, forVotes, againstVotes, ended, endingTime}) => {
    const [ providerAddress, setProviderAddress] = useState(null)

    const getProvider = async () => {
        const provider = await _getProvider()
        setProviderAddress(provider.currentProvider.selectedAddress)
    }

    useEffect(() => {
        getProvider()
    }, [null])

    return (
        <div>
            <h1>{name}</h1>
            <div>
                <VoteCount
                    label='For'
                    count={forVotes} />
                <VoteCount
                    label='Against'
                    count={againstVotes} />
            </div>
            {providerAddress ? <Vote address={providerAddress} proposalId={id} /> : null}
            <h3>{ended ? 'Voting Has Ended' : 'Voting Is Active'}</h3>
            <h4>{ended ? 'Ended' : 'Ending'} {endingTime}</h4>
        </div>
    )
}
