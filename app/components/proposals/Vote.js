import React from 'react'
import { useQuery } from 'graphql-hooks'
import ProposalVote from '../forms/ProposalVote'

const VOTING_RECORD_QUERY = `
  query votingRecord($proposalId: String, $address: String) {
    votingRecord(proposalId: $proposalId, address: $address)
  }
`


export default ({proposalId, address}) => {
    const { loading, error, data } = useQuery(VOTING_RECORD_QUERY, {variables:{proposalId, address }})
    console.log('voting record ', data)
    return (
        <div>
            {data && data.votingRecord ? 'You have already voted' :
                <ProposalVote proposalId={proposalId} />}
        </div>
    )
}