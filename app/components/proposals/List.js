import React from 'react'
import { useRouter } from 'next/router'

const ProposalItem = ({id, name, forVotes, againstVotes, ended, endingTime}) => {
    const router = useRouter()
    const href = `/proposals/${id}`

    const navToProposal = e => {
        e.preventDefault()
         
        router.push(href)
      }
    return (
        <div>
            <a href={href} onClick={navToProposal}>
                <h3>{name}</h3>
            </a>

        </div>
    )
}


export default ({proposals}) => proposals.length ?
    proposals.map(proposal => <ProposalItem key={proposal.id} {...proposal} />)
    :
    'No Proposals'
