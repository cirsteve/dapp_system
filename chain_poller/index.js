const Web3 = require('web3')
const abiDecoder = require('abi-decoder')
const ProposalVotingArtifact = require("../contracts/build/contracts/Proposals.json")
const graphQL = require('./graphql')

const GANACHE_NETWORK_ID = '5777'

// endpoint to listen for updates
const NODE_ENDPOINT = 'ws://127.0.0.1:9545'

const proposalVotingAddress = ProposalVotingArtifact.networks[GANACHE_NETWORK_ID].address

abiDecoder.addABI(ProposalVotingArtifact.abi)

const web3 = new Web3(NODE_ENDPOINT)

const getBlock = (hash) => web3.eth.getBlock(hash, true)

const getTransactionReceipt = (hash) => web3.eth.getTransactionReceipt(hash)

const decodeLogs = (logs) => abiDecoder.decodeLogs(logs)


// handle new ethereum blocks
const onNewBlockHeader = async (header) => {
    console.log('new header ', header)

    // get the block based on the has
    const block = await getBlock(header.hash)
    console.log('new block ', block)

    // get transaction receipts for all transactions in the block
    const transactions = await Promise.all(block.transactions.map(tx => getTransactionReceipt(tx.hash)))
    
    // filter over the transaction logs and return all logs emited by the ProposalVoting contract
    // and decode those logs
    const logs = transactions
        .reduce((acc, tx) => acc.concat(tx.logs), [])
        .filter(log => log.address === proposalVotingAddress)
        .map(log => {
            const decoded = decodeLogs([log])[0]
            console.log('deco ', decoded)
            return {
                blockHeight: block.number,
                blockTime: block.timestamp,
                txHash: log.transactionHash,
                address: log.address,
                name: decoded.name,
                args: JSON.stringify(decoded.events)
            }
        })
    const updatePayload = {
        blockHash: block.hash,
        blockHeight: block.number,
        logs: logs
    }

    await graphQL.updateBlock(updatePayload)
    
}

const listenForBlocks = () => {

    const subscription = web3.eth.subscribe('newBlockHeaders')
    subscription
        .on('connected', (id)=> console.log('connected to node ', id))
        .on('data', onNewBlockHeader)
        .on('error', console.error)
        console.log('listening for blocks')
    
    process.on('SIGINT', async function() {
        console.log("Caught interrupt signal");
        await subscription.unsubscribe(function(error, success){
            if(success)
                console.log('Successfully unsubscribed!');
        });
        process.exit()
    });
}

const start = async () => {
    await listenForBlocks()
}

start()

