const { GraphQLClient } = require('graphql-request')
// endpoint to send decodes updates
const APP_ENDPOINT = 'http://localhost:3000/api/graphql'

const client = new GraphQLClient(APP_ENDPOINT)

const updateBlockMutation = `
    mutation updateBlock(
        $blockUpdate: BlockUpdateInput!
    ) {
        updateBlock(
            blockUpdate: $blockUpdate
        )
    }
`

const updateBlock = (blockUpdate) => {
    const variables = { blockUpdate }
    console.log('got logs from chain ', JSON.stringify(variables))
    try {
        return client.request(updateBlockMutation, variables)
    } catch (e) {
        console.log('error updating block ', e.message)
    }
 }

module.exports = {
    updateBlock
}