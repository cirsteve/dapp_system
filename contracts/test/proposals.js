const Proposals = artifacts.require("Proposals");

contract('Proposals', (accounts) => {
  it('can create proposal and vote', async () => {
    const proposalInstance = await Proposals.deployed();
    const PROPOSAL_NAME = 'more dogs'
    await proposalInstance.createProposal(1582272000000, PROPOSAL_NAME, {from: accounts[0]});

    const proposal = await proposalInstance.getProposal(0)
    assert.equal(proposal.name, PROPOSAL_NAME, 'proposal name does not match' )
    
    proposalInstance.voteOnProposal(0, false, {from: accounts[0]} )

    const voteRecord = await proposalInstance.getVotingRecord(0, accounts[0])
    console.log('proposal in test ', voteRecord )
    assert.equal(
      voteRecord,
      true,
      "vote record should be true"
    );


  });

});
