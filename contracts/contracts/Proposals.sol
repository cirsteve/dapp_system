pragma solidity 0.6.2;

contract Proposals {

	// all relavant data for a proposal
	// supports only for/against vote
	// within a proposed deadline
	struct Proposal {
		uint id;
		uint endingTime;
		uint forVotes;
		uint againstVotes;
		address createdBy;
		string name;
	}

	event ProposalCreated(uint id, address createdBy, string name, uint endingTime);
	event VoteForProposal(address voter, uint proposalId, bool vote);

	// track all proposals created
	Proposal[] public proposals;

	// track votes to prevent double voting
	mapping(address => mapping(uint =>  bool)) public proposalVotes; 

	function createProposal(uint endingTime, string memory name) public {
		uint proposalId = proposals.length;
		proposals.push(Proposal(proposalId, endingTime, 0, 0, msg.sender, name));
		emit ProposalCreated(proposalId, msg.sender, name, endingTime);
	}

	// public function users can call to vote on a proposal
	// calling this with a 'true' vote is recorded as a 'forVote'
	// and calling with 'false' is recorded as an 'againstVote'
	function voteOnProposal(uint proposalId, bool vote) public {
		require(proposalId < proposals.length, 'Invalid proposal id');

		Proposal storage proposal = proposals[proposalId];
		bool votingRecord = proposalVotes[msg.sender][proposalId];
		require(block.timestamp < proposal.endingTime, 'Proposal voting has ended');
		require(votingRecord == false, 'Already voted for this proposal');

		proposalVotes[msg.sender][proposalId] = true;

		if (votingRecord == true) {
			proposal.forVotes = proposal.forVotes + 1;
		} else {
			proposal.againstVotes = proposal.againstVotes + 1;
		}

		emit VoteForProposal(msg.sender, proposalId, vote);

	}

	function getProposal(uint _id) public view returns (
		uint id,
		uint endingTime,
		uint forVotes,
		uint againstVotes,
		address createdBy,
		string memory name
		) {
			Proposal storage proposal = proposals[_id];
			id = proposal.id;
			endingTime = proposal.endingTime;
			forVotes = proposal.forVotes;
			againstVotes = proposal.againstVotes;
			createdBy = proposal.createdBy;
			name = proposal.name;
	}

	function getVotingRecord(uint proposalId, address voter) public view returns (bool) {
		return proposalVotes[voter][proposalId];
	}

}
